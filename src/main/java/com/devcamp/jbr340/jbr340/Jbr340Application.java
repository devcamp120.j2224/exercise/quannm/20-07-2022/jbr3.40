package com.devcamp.jbr340.jbr340;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr340Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr340Application.class, args);
	}

}
