package com.devcamp.jbr340.jbr340;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getAreaCircle() {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        return circle2.getArea();
    }

    // public static void main(String[] args) {
    //     Circle circle1 = new Circle();
    //     Circle circle2 = new Circle(2.5);
    //     System.out.println(circle1 + "," + circle2);
    //     System.out.println("Area circle1=" + circle1.getArea() + ",CV circle1=" + circle1.getCircumference());
    //     System.out.println("Area circle2=" + circle2.getArea() + ",CV circle2=" + circle2.getCircumference());
    // }
}
